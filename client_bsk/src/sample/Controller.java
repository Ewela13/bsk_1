package sample;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;


public class Controller {

    @FXML private PasswordField passwordField;
    @FXML private Pane receivePane;
    @FXML
    private Pane loginPane;
    @FXML
    private Pane waitForServerPane;

    @FXML
    private TextField fileNameBox;
    @FXML
    ProgressIndicator receiveProgress;

    @FXML
    private void startDownloadButton(ActionEvent event){
        loginPane.setVisible(true);
    }

    @FXML
    private void onEnterPassword(ActionEvent event) {
        BooleanProperty isReceivingState = new SimpleBooleanProperty(false);
        BooleanProperty isWaitingForServerState = new SimpleBooleanProperty(true);

        loginPane.setVisible(false);

        receivePane.visibleProperty().bind(isReceivingState);
        waitForServerPane.visibleProperty().bind(isWaitingForServerState);

        String passHash = Security.getHash(passwordField.getText());
        passwordField.setText("");
        DownloadFileTask download = new DownloadFileTask(passHash, isReceivingState, isWaitingForServerState, fileNameBox.getText());
        receiveProgress.progressProperty().bind(download.progressProperty());
        new Thread(download).start();

    }
}
