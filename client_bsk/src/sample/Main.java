package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;

import static sample.Security.*;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        generateAllRsaKeys();
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Client - Olek");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    private void generateAllRsaKeys() throws Exception{
        generateRsaKeys("Olek", "olek");
     //   generateRsaKeys("Ala", "ala");
     //   generateRsaKeys("Wojciech", "wojciech");
     //   generateRsaKeys("Kasia", "kasia");
    }

    private void generateRsaKeys(String name, String pass) throws Exception{
        byte[] iv = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair pair = keyGen.generateKeyPair();

        writeToFile("../server_bsk/"+name, pair.getPublic().getEncoded());

        byte[] rsaPriv = pair.getPrivate().getEncoded();

        byte[] keyBytes = getHash(pass).getBytes();

        Cipher c = Cipher.getInstance("DES/CBC/PKCS5Padding");
        DESKeySpec key = new DESKeySpec(keyBytes);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
        SecretKey desKey = skf.generateSecret(key);
        IvParameterSpec dps = new IvParameterSpec(iv);

        c.init(Cipher.ENCRYPT_MODE, desKey, dps);
        byte encrypted[] = c.doFinal(rsaPriv);

        writeToFile(name+"_private", encrypted);
    }


    private void writeToFile(String path, byte[] key) throws IOException {
        File f = new File(path);

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(key);
        fos.flush();
        fos.close();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
