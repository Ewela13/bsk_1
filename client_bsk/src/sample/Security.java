package sample;

import java.io.*;
import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;

public class Security {

     static String getHash(String text){

         try {
             MessageDigest mDigest  = MessageDigest.getInstance("SHA1");
             byte[] result = mDigest.digest(text.getBytes());
             StringBuffer sb = new StringBuffer();
             for (int i = 0; i < result.length; i++) {
                 sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
             }

             return sb.toString();
         } catch (NoSuchAlgorithmException e) {
             e.printStackTrace();
             return text;
         }

     }

    static byte[] decryptCBC(byte[] keyBytes, byte[] encryptedText){
         // odszyfrowanie hasła
        try {
            Cipher c = Cipher.getInstance("DES/CBC/PKCS5Padding");
            DESKeySpec key = new DESKeySpec(keyBytes);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
            SecretKey desKey = skf.generateSecret(key);
            byte[] iv = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};

            IvParameterSpec dps = new IvParameterSpec(iv);
            c.init(Cipher.DECRYPT_MODE, desKey, dps);
            System.out.println(encryptedText.length);

            return c.doFinal(encryptedText);
        }catch(Exception e){
            return null;
        }

    }

    static String decryptionRSA(byte[] data, byte[] key){
         // odszyfrowanie nagłówka
         try {
             System.out.println("KEY    ");
             System.out.println((key.length));
             PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(key);
             KeyFactory kf = KeyFactory.getInstance("RSA");
             PrivateKey privateKey = kf.generatePrivate(spec);
             Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
             cipher.init(Cipher.DECRYPT_MODE, privateKey);
             return new String(cipher.doFinal(data));

         }catch(Exception e){
             return new String(data);
         }
    }


    static OutputStream decryptFileDES(byte[] key, String fileName, byte[] vec, String mode) {
         // odszyfrowanie pliku
        try {
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
            SecretKey desKey = skf.generateSecret(dks);

            Cipher c;
            switch (mode) {
                case "ECB":
                    c = Cipher.getInstance("DES/" + mode + "/PKCS5Padding");
                    c.init(Cipher.DECRYPT_MODE, desKey);
                    break;
                case "CBC":
                case "CFB":
                case "OFB":
                    IvParameterSpec ivectorSpecv = new IvParameterSpec(vec);
                    c = Cipher.getInstance("DES/" + mode + "/PKCS5Padding");
                    c.init(Cipher.DECRYPT_MODE, desKey, ivectorSpecv);
                    break;
                default:
                    return null;
            }
            return new CipherOutputStream(new FileOutputStream(fileName), c);
        }catch(FileNotFoundException e) {
            e.printStackTrace();
           return null;
        }catch(Exception e){
            try {
                return new FileOutputStream(fileName);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                return null;
            }
        }

    }
}
