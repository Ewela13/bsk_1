package sample;

import java.io.FileOutputStream;
import java.io.OutputStream;
import javafx.beans.property.BooleanProperty;
import javafx.concurrent.Task;
import javax.crypto.CipherOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;

public class DownloadFileTask extends Task<Void> {

    private String passHash;
    private BooleanProperty isReceivingState;
    private BooleanProperty isWaitingForServerState;

    private String fileName;

    private enum Header{
        MODE, SIZE, EXTENSION, KEY, VECTOR
    }

    DownloadFileTask(String passHash, BooleanProperty isReceivingState, BooleanProperty isWaitingForServerState, String fileName){
        this.passHash = passHash;
        this.isReceivingState = isReceivingState;
        this.isWaitingForServerState = isWaitingForServerState;
        this.fileName = fileName;
    }

    private void saveRandomFile(String fileName){
        SecureRandom randomSecureRandom = new SecureRandom();

        int fileLen = Math.abs(randomSecureRandom.nextInt(1000));
        try {
            FileOutputStream fos = new FileOutputStream(fileName + ".a");
            byte[] content = new byte[2];
            for (int i = 0; i < fileLen; i++) {
                randomSecureRandom.nextBytes(content);
                fos.write(content);
                this.updateProgress(i, fileLen);
            }
            fos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private OutputStream saveFile(String header[]){

        System.out.println("\nDecrypted header: "+header[Header.EXTENSION.ordinal()]+header[Header.MODE.ordinal()]+header[Header.SIZE.ordinal()]);
        String mode;
        switch( Integer.parseInt(header[Header.MODE.ordinal()])){
            case 1:
                mode = "ECB";
                break;
            case 2:
                mode = "CBC";
                break;
            case 3:
                mode = "CFB";
                break;
            case 4:
                mode = "OFB";
                break;
            default:
                mode = "ECB";

        }
        OutputStream cos = Security.decryptFileDES(header[Header.KEY.ordinal()].getBytes(), fileName + "." + header[Header.EXTENSION.ordinal()],
                header[Header.VECTOR.ordinal()].getBytes(),mode );
        return cos;

    }

    protected boolean isEmpty(byte[] array){
        for(int i = 0; i < array.length; i++)
            if(array[i] != 0)
                return false;
        return true;

    }

    @Override
    protected Void call() throws Exception {
        Communication channel = new Communication();
        channel.openConnection();
        byte[] encryptedHeader = Arrays.copyOfRange(channel.receiveText(), 0, 256);

        byte[] encodedRSA = Files.readAllBytes(Paths.get(System.getProperty("user.dir")+"/Olek_private"));

        try {
            byte[] privateKey = Security.decryptCBC(passHash.getBytes(), encodedRSA);
            System.out.println("p_key2    "+new String(privateKey));
            String[] header = Security.decryptionRSA(encryptedHeader, privateKey).split(":");
            isReceivingState.setValue(true);
            System.out.println(Arrays.toString(header));
            OutputStream cos = saveFile(header);
            for (int i = 0; i < Integer.parseInt(header[Header.SIZE.ordinal()]); i++) {

                channel.receiveFile(cos);
                this.updateProgress(i, Integer.parseInt(header[Header.SIZE.ordinal()]));
            }

            cos.close();

        }catch(Exception e){
            byte[] file = channel.receiveText();
            int c = 0;
            while(!isEmpty(file)){

                c++;
                file = channel.receiveText();
            }
            System.out.println("Cii.."+String.valueOf(c));
            isReceivingState.setValue(true);

            saveRandomFile(fileName);

      //      channel.closeConnection();
      //      isReceivingState.setValue(false);
      ///      isWaitingForServerState.setValue(false);
       //     return null;
        }
        isReceivingState.setValue(true);

    /*    System.out.println("\nDecrypted header: "+header[Header.EXTENSION.ordinal()]+header[Header.MODE.ordinal()]+header[Header.SIZE.ordinal()]);
        String mode;
        switch( Integer.parseInt(header[Header.MODE.ordinal()])){
            case 1:
                mode = "ECB";
                break;
            case 2:
                mode = "CBC";
                break;
            case 3:
                mode = "CFB";
                break;
            case 4:
                mode = "OFB";
                break;
            default:
                mode = "ECB";

        }
        OutputStream cos = Security.decryptFileDES(header[Header.KEY.ordinal()].getBytes(), fileName + "." + header[Header.EXTENSION.ordinal()],
                header[Header.VECTOR.ordinal()].getBytes(),mode );*/
     /*   for (int i = 0; i < Integer.parseInt(header[Header.SIZE.ordinal()]); i++) {
            channel.receiveFile(cos);
            this.updateProgress(i, Integer.parseInt(header[Header.SIZE.ordinal()]));
        }

        cos.close();*/
      //  }catch(Exception e){
     //       e.printStackTrace();
     //   }
        channel.closeConnection();
        isReceivingState.setValue(false);
        isWaitingForServerState.setValue(false);
        return null;
    }
}
