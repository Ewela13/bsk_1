package sample;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Communication {

    final private int PORT = 13267;
    final private int BLOCK_SIZE = 1024;

    private ServerSocket servsock = null;
    private OutputStream os = null;
    private InputStream in = null;
    private Socket sock = null;

    public void openConnection() {
        try {
            servsock = new ServerSocket(PORT);
            sock = servsock.accept();
            os = sock.getOutputStream();
            in = sock.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] receiveText(){
        byte[] data = new byte[BLOCK_SIZE];
        try {
            in.read(data, 0, data.length);
            return data;
        } catch (IOException e) {
          //  e.printStackTrace();
            return null;
        }
            }


    void receiveFile(OutputStream output){
        int bytesRead;
        try {
            byte[] buffer = new byte[1024];
            if ((bytesRead = in.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    void closeConnection(){
        try {
            if (os != null) os.close();
            if (sock != null) sock.close();
            if (servsock != null) servsock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
