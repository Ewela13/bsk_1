package sample;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;

public class Security {


    static byte[] generateSessionKey(){
        SecureRandom randomSecureRandom = new SecureRandom();
        byte[] sess = new byte[52];
        randomSecureRandom.nextBytes(sess);
        for(int i = 0; i < 8; i++){
            if (sess[i] < 0)
                sess[i] = (byte)(sess[i]*-1);
        }
        return sess;
    }

    //szyfrowanie nagłówka
    public static byte[] encryptionRSA(String data, byte[] key){
        try {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(key);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey publicKey = kf.generatePublic(spec);

            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            System.out.println(data.getBytes().length);
            return cipher.doFinal(data.getBytes());
        }catch (Exception e){
            e.printStackTrace();
            return data.getBytes();
        }
    }

    // szyfrowanie pliku do wysłania
    static InputStream encryptDES(byte[] key, String fileName, byte[] vec, String mode){
        try {
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
            SecretKey desKey = skf.generateSecret(dks);
            Cipher c = Cipher.getInstance("DES/" + mode + "/PKCS5Padding");

            IvParameterSpec iv;
            switch (mode) {
                case "ECB":
                    c.init(Cipher.ENCRYPT_MODE, desKey);
                    break;
                case "CBC":
                case "CFB":
                case "OFB":
                    iv = new IvParameterSpec(vec);
                    c.init(Cipher.ENCRYPT_MODE, desKey, iv);
                    break;
                default:
                    return null;
            }
            return new CipherInputStream(new FileInputStream(fileName), c);
        }catch(FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }catch(Exception e){
            try {
                return new FileInputStream(fileName);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                return null;
            }
        }
    }

    static byte[] generateVector(){
        SecureRandom randomSecureRandom = new SecureRandom();
        byte[] iv = new byte[8];
        randomSecureRandom.nextBytes(iv);
        // zamiana ujemnych liczb na dodatnie
        for(int i = 0; i < 8; i++){
            if (iv[i] < 0)
                iv[i] = (byte)(iv[i]*-1);
        }
        return iv;
    }

    static byte[] getPublicKey(String person){
        String publicKeyFile = System.getProperty("user.dir")+"/"+person;
        try {
            return Files.readAllBytes(Paths.get(publicKeyFile));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
