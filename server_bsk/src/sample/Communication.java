package sample;

import java.io.*;
import java.net.Socket;

public class Communication {
    final private int PORT = 13267;
    final private int BLOCK_SIZE = 1024;
    final private String HOST = "127.0.0.1";

    private OutputStream os = null;
    private Socket sock = null;

    boolean openConnection() {
        try {
            sock = new Socket(HOST, PORT);
            os = sock.getOutputStream();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    void sendText(byte[] msg){
        try {
            os.write(msg, 0, msg.length);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void sendFile(InputStream in) {
        byte [] text  = new byte [BLOCK_SIZE];
        try {
         //   OutputStream out = sock.getOutputStream();
            int count;
            System.out.println("przed pęt;lą wysłania");
            int c=0;
            while ((count = in.read(text)) > 0) {
                c++;
                os.write(text, 0, count);
            }
            System.out.println("to  "+String.valueOf(c));


            os.flush();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    void closeConnection(){
        try {
            if (os != null) os.close();
            if (sock != null) sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
