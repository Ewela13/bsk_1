package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;

import java.io.*;



public class Controller {
    @FXML private Label filePathLabel;
    @FXML private ChoiceBox blockCipherChoiceBox;
    @FXML private  ChoiceBox personChoiceBox;

    @FXML
    private void choiceFileButton(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Wybierz plik do przesłania");
        File file = fileChooser.showOpenDialog(filePathLabel.getScene().getWindow());
        if (file != null) {
            filePathLabel.setText(file.getAbsolutePath());
        }
    }

    @FXML
    private void sendFileButton(ActionEvent event) {
        SendFileTask download = new SendFileTask(filePathLabel.getText(), blockCipherChoiceBox.getValue().toString(),
                personChoiceBox.getValue().toString());
        new Thread(download).start();
        }
}
