package sample;

import javafx.concurrent.Task;

import java.io.File;
import java.io.InputStream;

import static sample.Security.encryptDES;
import static sample.Security.generateVector;
import static sample.Security.getPublicKey;

public class SendFileTask extends Task<Void> {

    private String filePath;
    private String blockType;
    private String personName;

    SendFileTask(String path, String blockType, String personName){
        filePath = path;
        this.blockType = blockType;
        this.personName = personName;
    }

    @Override
    protected Void call() throws Exception {

        Communication channel = new Communication();
        //nic nie rób, jeżeli nie wybrano pliku lub nie ma połączenia
        if (filePath.equals("") || !channel.openConnection())
            return null;

        //tworzenie nagłówka
        File file = new File(filePath);
        byte[] sessionKey = Security.generateSessionKey();
        String extension = filePath.substring(filePath.lastIndexOf('.') + 1);
        String vector = new String(generateVector());
        int mode;
        switch(blockType){
            case "ECB":
                mode = 1;
                break;
            case "CBC":
                mode = 2;
                break;
            case "CFB":
                mode = 3;
                break;
            case "OFB":
                mode = 4;
                break;
            default:
                mode = 1;
        }
        String header = mode+":" + file.length() + ":" + extension + ":" + new String(sessionKey )+ ":" + vector;
        System.out.println(header);

        byte[] publicKey = getPublicKey(personName);
        System.out.println(new String(publicKey));
        //wysłanie nagłówka
        byte[] eHeader = Security.encryptionRSA(header, publicKey);
        System.out.println("to jest ta liczba"+eHeader.length);
        channel.sendText(eHeader);

        //wysyłanie pliku
        InputStream is = encryptDES(sessionKey, filePath, vector.getBytes(), blockType);
        channel.sendFile(is);
        channel.closeConnection();
        return null;
    }
}
